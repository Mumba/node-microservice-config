/**
 * MicroserviceConfigModule tests
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from "assert";
import {MicroserviceConfigModule} from '../../src/index';
import {DiContainer} from "mumba-typedef-dicontainer";
import {Config, FileLoader} from "mumba-config";

describe('MicroserviceConfigModule integration tests', () => {
	let container: DiContainer;

	beforeEach(() => {
		container = new Sandal();
	});

	it('should register "config" in the container', (done) => {
		MicroserviceConfigModule.register(container);

		assert.strictEqual(container.has('config'), true);

		container.resolve('config', (err: Error, config: Config) => {
			if (err) {
				return done(err);
			}

			assert(config instanceof Config, 'should be a Config object');
			done();
		});
	});

	it('should use "defaultConfig" if already set', (done) => {
		container.object('defaultConfig', { foo: 'bar' });

		MicroserviceConfigModule.register(container);

		container.resolve('config', (err: Error, config: Config) => {
			if (err) {
				return done(err);
			}

			assert.equal(config.get('foo'), 'bar', 'should set the default configuration');
			done();
		});
	});

	it('should apply any loaders during "configure"', (done) => {
		MicroserviceConfigModule.register(container);

		container.resolve((err: Error, config: Config) => {
			if (err) {
				return done(err);
			}

			// A Microservice will probably add other loaders in a "preConfiguration" step.
			config.registerLoader(new FileLoader());
			config.set('config:loaders', [
				{
					name: 'file',
					filePath: __dirname + '/config.js'
				}
			]);

			MicroserviceConfigModule.configure(config)
				.then(() => {
					assert.equal(config.get('beer'), 'free', 'should get configuration from the loader');
					done();
				})
				.catch(done);
		});
	});
});
