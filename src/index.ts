/**
 * index
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

export * from "mumba-config";
export {MicroserviceConfigModule} from "./MicroserviceConfigModule";
