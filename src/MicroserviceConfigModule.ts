/**
 * MicroserviceConfigModule
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {DiContainer} from "mumba-typedef-dicontainer";
import {Config} from "mumba-config";

/**
 * Registers "config" - A Config object.
 *
 * The Microservice can optionally add "defaultConfig" to the container but this must be done prior to registering the module.
 */
export class MicroserviceConfigModule {
	/**
	 * Register the Config assets in the DI container.
	 *
	 * @param {DiContainer} container
	 */
	static register(container: DiContainer) {
		if (!container.has('defaultConfig')) {
			container.object('defaultConfig', {});
		}

		container.service('config', ['defaultConfig'], Config);
	}

	/**
	 * Configures the configuration itself by applying additional loaders if available.
	 *
	 * @param {Config} config
	 * @returns {Promise<void>}
	 */
	static configure(config: Config): Promise<any> {
		let loaders: any[] = config.get('config:loaders') || [];

		return config.load(loaders);
	}
}
