# Mumba Config Microservice Module

Provide configuration support for a Microservice.

## Configuration

This module requires that a `Config` object has been registered as `config`.

Property | Type | Description
--- | :---: | ---
`vault:host` | `string` | The hostname for the Vault server. 

## DI Container

This module adds the following container artifacts if they don't already exist.

Name | Type | Description
--- | :---: | ---
`config` | `Config` | An instance of a [`mumba-config`](https://gitlab.com/Mumba/node-config) object.

The following container artifacts may be provided by the user.

Name | Type | Description
--- | :---: | ---
`defaultConfig` | `object` | An optional object that you provide representing the default configuration for a `Config` object. It must exist prior to the first initialisation of the `Config` object otherwise the vaules will not be injected.

## Installation

```sh
$ npm install --save mumba-microservice-config
$ typings install -—save dicontainer=npm:mumba-typedef-dicontainer
```

## Examples

```typescript
const Sandal = require("sandal");
import {DiContainer} from "mumba-typedef-dicontainer";
import {MicroserviceConfigModule} from "mumba-microservice-config";

let container: DiContainer = new Sandal();

// Set the default configuration.
container.object('defaultConfig', {
	foo: 'bar'
});

// Register the module.
MicroserviceConfigModule.register(container);

container.resolve('config', (err: Error, config: Config) => {
	if (err) {
		throw err;
	}

	let foo = config.get('foo'); // "bar"
});
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba Config Microservice Module_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-microservice-config/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

