# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.2.0] 19 Aug 2016
- Changed signature of `configure` to take the required arguments rather than the container.

## [0.1.2] 10 Aug 2016
- Added static `configure` method to handler loaders. 

## [0.1.1] 5 Aug 2016
- Fixed `package.json` links.
- Fixed up old typings format.
- Added exports for `mumba-config`.

## [0.1.0] 16 Jul 2016
- Initial release.
